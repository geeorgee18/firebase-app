import logo from './logo.svg';
import './App.css';
import { useEffect, useState } from 'react';
import {getTokenFirebase} from './Firebase/firebase'
getTokenFirebase()
function App() {
  const [count, setCount] = useState(0);

  useEffect(()=>{
    setCount(2)
  },[setCount])
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <button onClick={() => setCount(count + 1)}>
          Click me
        </button>
        <p>
          <code>{count} App</code> 
        </p>
      </header>
    </div>
  );
}

export default App;
