
import { initializeApp } from "firebase/app";
import { getToken, onMessage, getMessaging } from "firebase/messaging";

// Firebase Config
const firebaseApp = initializeApp({
    apiKey: "AIzaSyD_r89mgtlXPfGf4m8c-qmEEQQHVGXne4U",
    authDomain: "fire-app-practicas.firebaseapp.com",
    projectId: "fire-app-practicas",
    storageBucket: "fire-app-practicas.appspot.com",
    messagingSenderId: "867179044804",
    appId: "1:867179044804:web:4284afd5b949fc82918e91",
    measurementId: "G-LVQWFTMMFB"
});
// Get Messaging
const messaging = getMessaging(firebaseApp)

const {REACT_APP_VAPID_KEY} = process.env
const publicKey = REACT_APP_VAPID_KEY

// Get Token
export const getTokenFirebase = async()=>{
    let currentToken = ''

    try {
        currentToken = await getToken(messaging, {vapidKey:publicKey})
        if(currentToken){
            console.warn('success token',currentToken)
        } else {
            console.warn('failed token',currentToken)
        }
    } catch (error) {
        console.log('Ocurrio un error obteniendo el token', error)
    }
    return currentToken
}
// On Message
export const firstPlaneMessage = onMessage(messaging, (payload)=>{
    console.log('Message received. ', payload)
})
export const onMessageListener = () =>
    new Promise((resolve) => {
    messaging.onMessage((payload) => {
    resolve(payload);
    });
});