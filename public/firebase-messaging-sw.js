importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js');

const firebaseConfig = {
    apiKey: "AIzaSyD_r89mgtlXPfGf4m8c-qmEEQQHVGXne4U",
    authDomain: "fire-app-practicas.firebaseapp.com",
    projectId: "fire-app-practicas",
    storageBucket: "fire-app-practicas.appspot.com",
    messagingSenderId: "867179044804",
    appId: "1:867179044804:web:4284afd5b949fc82918e91",
    measurementId: "G-LVQWFTMMFB"
};

firebase.initializeApp(firebaseConfig);

// Retrieve firebase messaging
const messaging = firebase.messaging();

messaging.onBackgroundMessage(function(payload) {
    console.log('Received background message ', payload);

    const notificationTitle = payload.notification.title;
    const notificationOptions = {
    body: payload.notification.body,
    };

    self.registration.showNotification(notificationTitle,
    notificationOptions);
});